library(gWidgets2)
library(gWidgets2tcltk) 

convertFile = function(inputPath, outputPath) {
  # Rasclanjuje RINEX navigacijsku datoteku na lokaciji inputPath i 
  # rezultat zapisuje u csv formatu u datoteku na lokaciji outputPath
  #
  # Argumenti
  #  inputPath: put do ulazne datoteke i naziv datoteke
  #  outputPath: put do izlazne datoteke i naziv datoteke
  
  # Provjera argumenata
  if (is.null(inputPath) || nchar(inputPath) < 1) {
    stop("Nedostaje ulazna datoteka.");
  }
  if (is.null(outputPath) || nchar(outputPath) < 1) {
    stop("Nedostaje izlazna datoteka.");
  }
  
  # Kreiramo zaglavlje izlazne datoteke
  header <- c("Satellite PRN number", "Epoch year", "Epoch month", 
              "Epoch day","Epoch hour","Epoch minute","Epoch seconds", 
              "SV clock bias", "SV clock drift", "SV clock drift rate",
              "IODE","Crs","Delta n","M0",
              "Cuc","Eccentricity","Cus","sqrt(A)",
              "Toe","Cic","OMEGA","CIS",
              "i0","Crc","omega","OMEGA DOT",
              "IDOT","Codes on L2 channel","GPS Week #","L2 P data flag",
              "SV accuracy","SV health","TGD","IODC",
              "Transmission time of message","spare1","spare2","spare3"
  );
  outputLine <- formatLine(header);
  write(outputLine, file = outputPath, append = FALSE);
  
  # Spojimo se na ulaznu datoteku
  con = file(inputPath, "r");
  currentRow = 1;
  
  # Preskacemo header
  line = "";
  while (!grepl("END OF HEADER", line, fixed = TRUE)) {
    line = readLines(con, n = 1);
    if(length(line) ==0) {
      warning(
        "Nije pronadjen tekst 'END OF HEADER' u datoteci."
      );
    }
    currentRow <- currentRow + 1;
  }
  
  # Ucitavamo po 1 red do kraja datoteke
  continue = TRUE;
  while(continue) {
    # Vektor parameters sadrzi vrijednosti koje ce 
    # popuniti jedan red izlazne datoteke
    parameters <- character(length = 38);  
    for (i in 0 : 7) {
      line = readLines(con, n = 1)
      
      # Ako ne mozemo ucitati sljedeci red, izlazimo iz petlje
      if(length(line) == 0) {
        continue = FALSE;
        break;
      }
      
      if (i == 0) {
        # Citamo parametre PRN / EPOCH / SV CLK
        row1 <- readPrnEpochSvClk(line);
        if (is.null(row1)) {
          continue = FALSE;
          warning(paste(
            "Greska u citanju navigacijske poruke, red: ", 
            toString(currentRow)));
          break;
        }
        for (j in (1 : 10)) {
          parameters[j] <- row1[j]; 
        }
      } else {
        if (i < 7) {
          # Citamo BROADCAST ORBIT parametre
          rowOrbit <- readBroadcastOrbit(line);
        } else {
          # Citamo zadnji BROADCAST ORBIT parametar
          rowOrbit <- tryCatch({
            readBroadcastOrbitLast(line);
          }, warning = function(w) {
            warn(paste("Neocekivani format reda: "), currentRow);
          });
        }
        if (is.null(rowOrbit)) {
          continue = FALSE;
          warning(paste(
            "Greska u citanju navigacijske poruke, red: ", 
            toString(currentRow)));
          break;
        }
        for (j in (0 : 3)) {
          k <- 7 + 4 * i + j;
          parameters[k] <- rowOrbit[j + 1];
        }
      }
      currentRow <- currentRow + 1;
    }
    if (continue) {
      # Pretvorimo vektor s rasclanjenim vrijednostima u string
      outputLine <- formatLine(parameters); 
      # Zapisemo string u izlaznu datoteku
      write(outputLine, file = outputPath, append = TRUE);
    }
  }
  
  close(con);
}

readPrnEpochSvClk <- function(line) {
  # Rasclanjuje prvi red navigacijske poruke 
  # 
  # Argumenti
  #   line: prvi red navigacijske poruke
  # Vraca
  #   result: vektor duzine 10 koji se sastoji od 
  #   podataka iz prvog reda navigacijske poruke
  if (nchar(line) < 79) {
    return(NULL);
  }
  result <- character(length = 10);
  result[1] <- trim(substr(line, 1, 2));
  for (i in 1:5) {
    result[i+1] <- trim(substr(line, i * 3, i * 3 + 2));
  }
  result[7] <- trim(substr(line, 18, 22));
  for (i in 0:2) {
    result[i+8] <- trim(substr(line, i * 19 + 23,
                               (i + 1) * 19 + 22))
  }
  return(result);
}

readBroadcastOrbit <- function(line) {
  # Rasclanjuje jedan red navigacijske poruke 
  # izmedju drugog i sedmog reda ukljucivo
  # 
  # Argumenti
  #   line: red navigacijske poruke
  # Vraca
  #   result: vektor duzine 10 koji se sastoji od 
  #   podataka iz reda navigacijske poruke
  if (nchar(line) < 79) {
    return(NULL);
  }
  result <- character(length = 4); 
  for (i in 0 : 3) {
    result[i + 1] <- trim(substr(line, 4 + i * 19, 3 + (i + 1) * 19));
  }
  return(result);
}

readBroadcastOrbitLast <- function(line) {
  # Rasclanjuje osmi red navigacijske poruke 
  # 
  # Argumenti
  #   line: osmi red navigacijske poruke
  # Vraca
  #   result: vektor koji se sastoji od 
  #   podataka iz reda navigacijske poruke
  
  # Odredimo koliko zapisa ima u redu
  chars <- nchar(line) - 3;
  nValues <- floor(chars / 19);
  
  if (nValues > 4) {
    nValues = 4;
  }
  
  if (chars %% 19 != 0 || chars > 79) {
    warn("Neocekivani format poruke.");
  }
  
  result <- character(length = nValues); 
  for (i in 0 : nValues-1) {
    result[i + 1] <- trim(substr(line, 4 + i * 19, 3 + (i + 1) * 19));
  }
  if (nValues < 4) {
    for (i in nValues : 3) {
      result[i + 1] <- "0";
    }
  }
  return(result);
}

formatLine <- function(values) paste(values, collapse=",")

trim <- function (value) gsub("^\\s+|\\s+$", "", value)

inputFile <- file.choose(new = FALSE)
outputFile <- file.choose(new = TRUE)
convertFile(inputFile, outputFile)

